package by.epam.rd.sayhi;

public class SayHello {
    public static void main(String[] args) {
        Printer printer = new Printer();
        if (args.length == 1) {
            printer.print("Hello, " + args[0] + "!");
        } else {
            printer.print("Wrong argument amount: should be exactly one.");
        }
    }
}
